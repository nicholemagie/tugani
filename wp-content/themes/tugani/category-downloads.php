<?php
/**
 * Created by PhpStorm.
 * User: awbelisk07
 * Date: 12/26/15
 * Time: 12:24 PM
 */
?>

<div class="category-main">
    <h3>Headline Category</h3>
    <?php
    query_posts("category_name=downloads&posts_per_page=-1");
    if (have_posts()){
        while (have_posts()) {
            the_post();
            echo '<p>'. the_title() .'</p>';
            if ( has_post_thumbnail() ) {
                the_post_thumbnail();
            }
            the_content();
        }
    }
    ?>
</div>