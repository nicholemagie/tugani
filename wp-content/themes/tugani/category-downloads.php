<?php
/**
 * Created by PhpStorm.
 * User: awbelisk07
 * Date: 12/26/15
 * Time: 12:24 PM
 */
?>
<div id="page-header" class="col-md-2">
    <?php get_header() ?>
</div>
<div class="col-md-8" id="page-body">
    <div class="navbar navbar-default" id="nav-bar" role="navigation">
         <div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav" id="navbar-nav">
                    <?php
                    $args = array(
                        'hide_empty' => FALSE
                    );
                    $categories = get_categories($args);
                    foreach($categories as $category) {
                        if($category->name != 'Uncategorized') {
                            $cat_id = get_cat_ID($category->name);
                            $cat_link = get_category_link($cat_id);
                            if($category->name == 'Downloads')
                                echo '<li ><a class="active" href="' . $cat_link . '">' . $category->name . '</a></li>';
                            else
                                echo '<li><a href="' . $cat_link . '">' . $category->name . '</a></li>';
                        }
                    }
                    
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="category-main">
        <h3>Headline Category</h3>
        <?php
        query_posts("category_name=downloads&posts_per_page=-1");
        if (have_posts()){
            while (have_posts()) {
                the_post();
                echo '<p>'. the_title() .'</p>';
                if ( has_post_thumbnail() ) {
                    the_post_thumbnail();
                }
                the_content();
            }
        }
        ?>
    </div>
</div>
<div id="page-footer" class="col-md-2">
    <?php get_sidebar(); ?>
</div>