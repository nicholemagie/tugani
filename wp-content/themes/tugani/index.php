<?php get_header() ?>

<div>
    <div id="content" class="container col-md-8">
        <div id="nav-bar" class="row">
            <?php
            $args = array(
                'hide_empty' => FALSE
            );
            $categories = get_categories($args);
            foreach($categories as $category) {
                if($category->name != 'Uncategorized') {
                    $cat_id = get_cat_ID($category->name);
                    $cat_link = get_category_link($cat_id);
                    echo '<div class="col-md-2"><a href="' . $cat_link . '">' . $category->name . '</a></div>';
                }
            }
            ?>
        </div>
        <div id="main_headline">
            <h3>Top Headline</h3>
            <?php
            $args = array(
                'posts_per_page' => 1,
                'offset' => 0,
                'category' => '',
                'category_name' => 'headline',
                'orderby' => 'date',
                'order' => 'DESC',
                'include' => '',
                'exclude' => '',
                'meta_key' => '',
                'meta_value' => '',
                'post_type' => 'post',
                'post_mime_type' => '',
                'post_parent' => '',
                'author' => '',
                'post_status' => 'publish',
                'suppress_filters' => true
            );
            $top_headline = wp_get_recent_posts($args);
            foreach($top_headline as $headline){
                echo '<a href="'. get_permalink($headline['ID']) .'">' . $headline['post_title'] . '</a>';
            }
            ?>
        </div>
        <div id="recent_headlines">
            <h3>Recent Headlines</h3>
            <?php
            $args = array(
                'posts_per_page' => 6,
                'offset' => 0,
                'category' => '',
                'category_name' => 'headline',
                'orderby' => 'date',
                'order' => 'DESC',
                'include' => '',
                'exclude' => '',
                'meta_key' => '',
                'meta_value' => '',
                'post_type' => 'post',
                'post_mime_type' => '',
                'post_parent' => '',
                'author' => '',
                'post_status' => 'publish',
                'suppress_filters' => true
            );
            $top_headline = wp_get_recent_posts($args);
            $i = 1;

            foreach($top_headline as $headline){
                if($i != 1)
                    echo '<p><a href="' . get_permalink($headline['ID'])  . '">' . $headline['post_title'] . '</a></p>';
                $i++;
            }
            ?>
        </div>
    </div>
    <div id="right" class="col-md-2">
        <?php get_sidebar(); ?>
    </div>
</div>

<!--?php get_sidebar() ? ->
<!--?php get_footer() ? ->