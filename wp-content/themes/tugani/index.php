<div id="page-header" class="col-md-2">
    <?php get_header() ?>
</div>
<div class="col-md-8" id="page-body">
    <div class="navbar navbar-default" id="nav-bar" role="navigation">
         <div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav" id="navbar-nav">
                    <?php
                    $args = array(
                        'hide_empty' => FALSE
                    );
                    $categories = get_categories($args);
                    foreach($categories as $category) {
                        if($category->name != 'Uncategorized') {
                            $cat_id = get_cat_ID($category->name);
                            $cat_link = get_category_link($cat_id);
                            echo '<li><a href="' . $cat_link . '">' . $category->name . '</a></li>';
                        }
                    }
                  
                    ?>
                </ul>
            </div>
        </div>
    </div>
        <div id="main_headline">
            <?php
                $args = array(
                    'posts_per_page' => 1,
                    'offset' => 0,
                    'category' => '',
                    'category_name' => 'headline',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'include' => '',
                    'exclude' => '',
                    'meta_key' => '',
                    'meta_value' => '',
                    'post_type' => 'post',
                    'post_mime_type' => '',
                    'post_parent' => '',
                    'author' => '',
                    'post_status' => 'publish',
                    'suppress_filters' => true
                );
                $top_headline = wp_get_recent_posts($args);
                foreach($top_headline as $headline){
                    echo "<div class='col-md-8' id='headline'><img class='img-responsive center-block' src='" . get_the_post_thumbnail_url($headline['ID']) . "' id='image_headline' ></div>";
                    echo '<div class="col-md-4" id="content"><h3 style="float: right"><a href="'. get_permalink($headline['ID']) .'">' . $headline['post_title'] . '</a></h3></div>';
                    echo '<div id="text">' . substr($headline['post_content'], 0, 200);
                    echo '<a id="see_full_text" href="' . get_permalink($headline['ID']) . '">>>></a>' . '</div>';
                }
            ?>
        </div>
        <div id="recent_headlines">
            <?php
            $args = array(
                'posts_per_page' => 6,
                'offset' => 0,
                'category' => '',
                'category_name' => 'headline',
                'orderby' => 'date',
                'order' => 'DESC',
                'include' => '',
                'exclude' => '',
                'meta_key' => '',
                'meta_value' => '',
                'post_type' => 'post',
                'post_mime_type' => '',
                'post_parent' => '',
                'author' => '',
                'post_status' => 'publish',
                'suppress_filters' => true
            );
            $top_headline = wp_get_recent_posts($args);
            $i = 1;

            foreach($top_headline as $headline){
                if($i != 1) {
                    echo "<div id='recent_hl . $i'  class='col-md-4'><img id='recent_hl' src='" . get_the_post_thumbnail_url($headline['ID']) . "' class='img-responsive center-block' >";
                    echo '<h3><a href="' . get_permalink($headline['ID']) . '">' . $headline['post_title'] . '</a></h3>';
                    echo  substr($headline['post_content'], 0, 100);
                    echo '<a id="see_full_text" href="' . get_permalink($headline['ID']) . '">>>></a>' . '</div>';
                }
                $i++;
            }
            ?>
        </div>
</div>
<div id="page-footer" class="col-md-2">
    <?php get_sidebar(); ?>
</div>


<!--?php get_sidebar() ? ->
<!--?php get_footer() ? ->