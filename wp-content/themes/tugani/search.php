<section id="primary" class="content-area">
    <div id="content" class="site-content" role="main">

        <?php while (have_posts()) : the_post(); ?>
            <h1>Search Results</h1>
            <!-- Needs something here -->
            <a href="<?php the_permalink() ?>">
                <h2><?php the_title(); ?></h2>
            </a>
            <p><?php the_excerpt(); ?></p>

        <?php endwhile; ?>
    </div>
</section>