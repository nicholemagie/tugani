<?php
/**
 * Created by PhpStorm.
 * User: awbelisk07
 * Date: 12/26/15
 * Time: 12:24 PM
 */
?>
<div id="page-header" class="col-md-2">
    <?php get_header() ?>
</div>
<div class="col-md-8" id="page-body">
    <div class="navbar navbar-default" id="nav-bar" role="navigation">
         <div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav" id="navbar-nav">
                    <?php
                    $args = array(
                        'hide_empty' => FALSE
                    );
                    $categories = get_categories($args);
                    foreach($categories as $category) {
                        if($category->name != 'Uncategorized') {
                            $cat_id = get_cat_ID($category->name);
                            $cat_link = get_category_link($cat_id);
                            if($category->name == 'Downloads')
                                echo '<li ><a class="active" href="' . $cat_link . '">' . $category->name . '</a></li>';
                            else
                                echo '<li><a href="' . $cat_link . '">' . $category->name . '</a></li>';
                        }
                    }
                    
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div>
	    <?php
		    $curr_post = get_post(get_the_ID());
		    $e = get_the_post_thumbnail_url(get_post(get_the_ID()));
		    echo '<div  id="title"><h3>' . $curr_post->post_title . '</h3>';
		    echo '<h6>' . $curr_post->post_date . '</h6>';
		    echo '<img class="img-responsive center-block" src="' . $e . '">';
		    echo '<div id="post_text">' . $curr_post->post_content . '</div></div>';
	    ?>

	</div>
	<form class="form-inline">
		<div class="form-group">
			<label for="email">Email Address</label>
			<input type="email" class="form-control" id="email" placeholder="Email">
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" class="form-control" id="password" placeholder="Password">
		</div>
	</form>
	<form>
		<div class="form-group">
			<label for="comment">Comment</label>
			<textarea class="form-control" id="comment" rows="3" placeholder="Comment"></textarea>
		</div>
		<button type="submit" class="btn btn-default" id="submit">Submit</button>
	</form>
</div>
<div id="page-footer" class="col-md-2">
    <?php get_sidebar(); ?>
</div>
