<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php endif; ?>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" src="style.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <?php wp_head(); ?>
</head>
<body <?php body_class();?> id="body">
    <div id="left">
        <img src="/tugani/wp-content/themes/tugani/tugani-logo-white.png" class="img-responsive" alt="Responsive Image">
    </div>
    <div id="banner" class="jumbotron">
        <div class="container">
            <h2>Official Tug-Ani Website</h2>
            <p>Any article here is written by the staff of Tug-ani</p>
        </div>
    </div>
    
</body>
</html>