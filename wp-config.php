<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tugani');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2AC 0jT3e@:Q.ZoeaA-s69br{7XR4kK|7*S=-$.k;]Y}|#Bbl9|cf~ZV|nCKXHVJ');
define('SECURE_AUTH_KEY',  'GP +Qo$I|TdXimR;FQaV?)lpDnJ(c<i9rl9.G^yxr 5QJ|5j-v0MN%GI ` 6@;Pk');
define('LOGGED_IN_KEY',    'y~iI13Y}+t[BD0Q.X:p`G;L|W[~R|*L#ZY+S+i/;mZp(nmH4iB/Pn<R^*Yvzvk6j');
define('NONCE_KEY',        ',Z8VL-}YuQg)Qmggez&+9DRb#1e&9O$Yu 2X^au5u0FSX7~6uY0$cMi5@#^ #=~o');
define('AUTH_SALT',        'Kh+Detu0+xptxc]Pf||M5UM):U(D~___`:9Y*._<adG+jg;iaj1joC4[o{1&y?yG');
define('SECURE_AUTH_SALT', 'X|H<edxo+6_i,B^ZDG+gL2-Rpd~`,NdK:<Ksq3N5MO;A;:UA-o6Bt:;7]a6z1R}-');
define('LOGGED_IN_SALT',   '`1YTaXC0*PY_{?ouR3+=+>?aRg36Zub4nW&I{BTWreM_C-)/+XL}Ot7,N}sWd6=v');
define('NONCE_SALT',       'By)S_`t7~>F{F)g+^BRXj-?(S[t[YGxaIx|N[36KQ%IB$d7* tOflz[s1dF^ Jg*');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tugani_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
